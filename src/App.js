import { Link, Route, Routes } from "react-router-dom"
import "antd/dist/antd.less"
import Dsvb_user from "./components/Pages/Dsvb_user"
import Da_gui from "./components/Pages/Da_gui"
import Gui_den from "./components/Pages/Gui_den"
import Vb_user_tao from "./components/Pages/Vb_user_tao"
import Vb_user_duyet from "./components/Pages/Vb_user_duyet"
import Vb_user_extra from "./components/Pages/Vb_user_extra"
import Dsvb_quanly from "./components/Pages/Dsvb_quanly"
import Dscc_quanly from "./components/Pages/Dscc_quanly"
import Vbct_quanly from "./components/Pages/Vbct_quanly"
import Taoquytrinh from "./components/Pages/Taoquytrinh"
import "./App.css"
import { getTemplates, getTemplateById } from "./redux/slices/templateSlice"
import { useDispatch, useSelector } from "react-redux"
import {
    getTemplateSelector,
    getFolderSelector,
    getTemplateSelectorById,
    getStepsByIdSelector,
} from "./redux/selectors"
import { useEffect } from "react"
import { getFolder } from "./redux/slices/folderSlice"
import { Layout } from "antd"
const { Header } = Layout
import { Logout } from "./assets/images"
import Sidebar from "./components/SideBar/index"
import Dang_nhap from "./components/Pages/Dang_nhap"

function App() {
    // const dispatch = useDispatch()
    // useEffect(() => {
    // dispatch(getTemplates())
    // dispatch(getTemplateById(1))
    // dispatch(getFolder())
    // dispatch(getStepsById(1))
    // }, [dispatch])

    // const templateList = useSelector(getTemplateSelector)
    // const folderList = useSelector(getFolderSelector)
    // const template = useSelector(getTemplateSelectorById)
    // const steps = useSelector(getStepsByIdSelector)
    // console.log(folderList)
    // console.log(template)
    // console.log(steps)
    // const templateList = useSelector(getTemplateSelector)

    return (
        <>
            <Sidebar />
            <div style={{ flex: 1 }}>
                <div>
                    <Layout className="site-layout">
                        <Header className="header">
                            <div className="header__user-wrapper">
                                <div className="header__user-avatar"></div>
                                <span className="header__user-name">
                                    Chào, <strong>vinhnq</strong>
                                </span>
                                <div className="header__user-logout">
                                    <Link to="/login">
                                        <img src={Logout} alt="" />
                                    </Link>
                                </div>
                            </div>
                        </Header>
                    </Layout>
                </div>
                <Routes>
                    <Route path="/login" element={<Dang_nhap />} />
                    {/* list templates, create templates */}
                    <Route path="/" exact element={<Dsvb_quanly />} />
                    <Route path="/vbct" element={<Vbct_quanly />} />
                    <Route path="/edit_template" element={<Vbct_quanly />} />
                    <Route path="/dscc" element={<Dscc_quanly />} />
                    <Route path="/taoqt" element={<Taoquytrinh />} />
                    <Route path="/ud" element={<Vb_user_duyet />} />
                    <Route path="/ut" element={<Vb_user_tao />} />
                    <Route path="/ue" element={<Vb_user_extra />} />
                    <Route path="/u" element={<Dsvb_user />} />
                    <Route path="/dagui" element={<Da_gui />} />
                    <Route path="/guiden" element={<Gui_den />} />
                </Routes>
            </div>
        </>
    )
}

export default App
