import $ from "jquery"
import { Plus } from "../../../assets/images"
import { Breadcrumb, Button, Input, Modal, Select, message } from "antd"
import "./Vbct_quanly.less"
import { Close } from "../../../assets/images"
import { useState, createRef, useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import {
    getEditingTemplateSelector,
    getFolderSelector,
} from "../../../redux/selectors"
import { createTemplate } from "../../../redux/slices/templateSlice"
import { createFolder } from "../../../redux/slices/folderSlice"
import { Link, useNavigate } from "react-router-dom"

window.jQuery = $ // JQuery alias
window.$ = $

require("jquery-ui-sortable")
require("formBuilder")
require("formBuilder/dist/form-render.min.js")

function Vbct_quanly() {
    const dispatch = useDispatch()
    let formBuilder
    const currentPath = window.location.pathname
    const navigate = useNavigate()

    const fb = createRef()
    const createBtn = createRef()
    const { TextArea } = Input

    const [isModalVisible, setIsModalVisible] = useState(false)
    const [folderSelected, setFolderSelected] = useState()
    const [jsonData, setJsonData] = useState("")
    const [addFolderInput, setAddFolderInput] = useState("")
    const [templateNameInput, setTemplateNameInput] = useState("")
    const [templateDescInput, setTemplateDescInput] = useState("")
    const folderList = useSelector(getFolderSelector)
    const editingTemplate = useSelector(getEditingTemplateSelector)

    const dfValueFolder =
        currentPath == "/edit_template"
            ? { defaultValue: editingTemplate.folder_name }
            : { placeholder: "Chọn nhóm" }
    const dfValueJsonData =
        currentPath == "/edit_template" ? editingTemplate.jsondata : []

    const config = {
        defaultFields: dfValueJsonData,
        i18n: {
            override: {
                "en-US": {
                    // getStarted: 'Kéo một trường từ bên phải vào vùng này',
                    addOption: "Thêm tùy chọn",
                    allFieldsRemoved: "Tất cả các trường đã bị gỡ bỏ.",
                    allowSelect: "Cho phép chọn",
                    cannotBeEmpty: "Trường này không thể để trống",
                    class: "Class",
                    clearAllMessage: "Bạn có chắc muốn xóa tất cả các trường?",
                    clear: "Xóa",
                    close: "Đóng",
                    content: "Nội dung",
                    copy: "Sao chép vào bộ nhớ",
                    dateField: "Trường thời gian",
                    description: "Văn bản trợ giúp",
                    descriptionField: "Mô tả",
                    devMode: "Chế độ nhà phát triển",
                    editNames: "Sửa tên",
                    editorTitle: "Các phần tử biểu mẫu",
                    editXML: "Sửa XML",
                    fieldVars: "Field Variables",
                    fieldNonEditable: "Trường này không thể sửa",
                    fieldRemoveWarning: "Bạn có chắc muốn xóa trường này?",
                    fileUpload: "Tải lên tập tin",
                    formUpdated: "Biểu mẫu đã cập nhật",
                    getStarted: "Kéo một trường từ bên phải vào vùng này",
                    header: "Tiêu đề",
                    hide: "Sửa",
                    hidden: "Input ẩn",
                    label: "Nhãn",
                    labelEmpty: "Nhãn không thể để trống",
                    limitRole:
                        "Giới hạn truy cập vào một trong những quyền sau:",
                    mandatory: "Bắt buộc",
                    maxlength: "Độ dài tối đa",
                    minOptionMessage: "Trường này yêu cầu tối thiểu 2 tùy chọn",
                    name: "Tên",
                    no: "Không",
                    off: "Tắt",
                    on: "Bật",
                    option: "Tùy chọn",
                    optional: "tùy chọn",
                    optionEmpty: "Giá trị của tùy chọn là bắt buộc",
                    paragraph: "Đoạn văn bản",
                    placeholder: "Placeholder",
                    preview: "Xem trước",
                    radioGroup: "Nhóm Radio",
                    radio: "Radio",
                    removeMessage: "Gỡ bỏ phần tử",
                    remove: "&#215;",
                    required: "Bắt buộc",
                    richText: "Trình soạn thảo văn bản có định dạng",
                    roles: "Truy cập",
                    save: "Lưu lại",
                    selectOptions: "Các tùy chọn",
                    select: "Chọn",
                    selectColor: "Chọn màu",
                    selectionsMessage: "Cho phép nhiều lựa chọn",
                    size: "Kích cỡ",
                    sizes: "Kích cỡ",
                    style: "Style",
                    styles: "Styles",
                    subtype: "Loại",
                    text: "Trường văn bản",
                    textArea: "Vùng văn bản",
                    toggle: "Bật tắt",
                    warning: "Cảnh báo!",
                    viewJSON: "{ }",
                    viewXML: "</>",
                    yes: "Đồng ý",
                },
            },
        },
        controlOrder: ["header", "text", "textarea", "date", "number", "file"],
        replaceFields: [
            {
                type: "text",
                label: "Trường văn bản",
            },
            {
                type: "header",
                label: "Tiêu đề",
            },
            {
                type: "textarea",
                label: "Vùng văn bản",
            },
            {
                type: "date",
                label: "Chọn ngày tháng",
            },
            // {
            //     type: "select",
            //     label: "Lựa chọn",
            // },
            // {
            //     type: "checkbox-group",
            //     label: "Các lựa chọn",
            // },
            // {
            //     type: "file",
            //     label: "Đính kèm tập tin",
            // },
            {
                type: "number",
                label: "Số",
            },
        ],
        disableFields: [
            "autocomplete",
            "button",
            "radio-group",
            "paragraph",
            "hidden",
            "select",
            "checkbox-group",
            "file",
        ],
        disabledActionButtons: ["clear", "save", "data"],
    }

    useEffect(() => {
        // khởi tạo formBuilder ở lần chạy đầu tiên
        jQuery(function ($) {
            formBuilder = $(fb.current).formBuilder(config)
            createBtn.current.addEventListener("click", function () {
                setJsonData(formBuilder.actions.getData("json"))
            })
        })
    }, [])

    const handleCreateTemplate = (name, folder_id, desc) => {
        dispatch(
            createTemplate({
                folder_ID: folder_id,
                desc: desc,
                jsondata: JSON.parse(jsonData),
            })
        )
    }

    const handleSelectFolder = (value) => {
        setFolderSelected(value)
    }

    const showModal = () => {
        setIsModalVisible(true)
    }

    const handleOk = () => {
        if (addFolderInput !== "") {
            dispatch(
                createFolder({
                    name: addFolderInput,
                })
            )

            setAddFolderInput("")
            setIsModalVisible(false)
        } else
            message.error({
                content: "Hãy nhập một tên nhóm !",
                // className: "message",
            })
    }

    const handleCancel = () => {
        setIsModalVisible(false)
    }

    const handleSubmit = () => {
        if (!templateNameInput || !templateDescInput || !folderSelected) {
            message.error({
                content: "Hãy nhập đầy đủ các trường thông tin.",
                // className: "message",
            })
        } else if (jsonData == "[]") {
            message.error({
                content: "Hãy tạo biểu mẫu cho tài liệu này.",
                // className: "message",
            })
        } else {
            let folderID
            folderList.forEach((folder) => {
                if (folder.name == folderSelected) {
                    folderID = folder.id
                }
            })

            // Xóa form cũ tạo lại form mới

            setTemplateNameInput("")
            setTemplateDescInput("")
            setFolderSelected()

            handleCreateTemplate(templateNameInput, folderID, templateDescInput)
        }
    }

    const handleChangeAddFolderInput = (e) => {
        setAddFolderInput(e.target.value)
    }

    const handleCancelCreate = () => {
        navigate("/")
    }

    return (
        <div>
            <Breadcrumb className="breadcrumb" separator="»">
                <Breadcrumb.Item>Tài liệu</Breadcrumb.Item>
                <Breadcrumb.Item>
                    <Link to="/">Danh sách tài liệu</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Tạo tài liệu</Breadcrumb.Item>
            </Breadcrumb>
            <div className="vbctql-layout-background">
                <h3 className="title">Tạo tài liệu</h3>

                <div className="vbctql-content">
                    <div className="vbctql-form">
                        <div className="vbctql-form__left">
                            <div>
                                <Select
                                    // mode="tags"
                                    style={{
                                        width: "100%",
                                    }}
                                    onChange={handleSelectFolder}
                                    // onSelect= {handleSelectFolder}
                                    value={folderSelected}
                                    // filterOption={(input, option) => {
                                    //     (option.children.toLowerCase().includes(input.toLowerCase()))
                                    // }}

                                    {...dfValueFolder}
                                    // placeholder="Chọn nhóm"
                                    optionLabelProp="label"
                                    listHeight={128}
                                    showSearch
                                    className="vbctql-form__folder"
                                >
                                    {folderList.map((folder, index) => {
                                        return (
                                            <Select.Option
                                                key={index}
                                                value={folder.name}
                                            ></Select.Option>
                                        )
                                    })}
                                </Select>

                                {/* <Input className="vbctql-form__folder" placeholder="Tên nhóm"/> */}
                                <Button
                                    type="primary"
                                    className="vbctql-form__folder-add"
                                    onClick={showModal}
                                >
                                    <img src={Plus} alt="" />
                                </Button>
                            </div>
                            <Input
                                className="vbctql-form__template"
                                placeholder="Tên tài liệu"
                                value={
                                    currentPath == "/edit_template"
                                        ? editingTemplate.desc
                                        : templateNameInput
                                }
                                onChange={(e) =>
                                    setTemplateNameInput(e.target.value)
                                }
                            />
                        </div>
                        <div className="vbctql-form__right">
                            <TextArea
                                value={
                                    currentPath == "/edit_template"
                                        ? editingTemplate.desc
                                        : templateDescInput
                                }
                                onChange={(e) =>
                                    setTemplateDescInput(e.target.value)
                                }
                                className="vbctql-form__desc"
                                placeholder="Mô tả"
                                autoSize={{ minRows: 3, maxRows: 5 }}
                            />
                        </div>
                    </div>

                    <div className="fb-wrapper" ref={fb}></div>

                    <div className="vbctql-actions">
                        <Button className="btn" onClick={handleCancelCreate}>
                            Hủy
                        </Button>
                        <Button
                            className="btn btn_create"
                            type="primary"
                            ref={createBtn}
                            onClick={handleSubmit}
                        >
                            Tạo
                        </Button>
                    </div>
                </div>
            </div>

            <Modal
                visible={isModalVisible}
                // onOk={handleOk}
                onCancel={handleCancel}
                centered={true}
                className="vbctql__modal"
                footer={null}
                closeIcon={<img src={Close} />}
            >
                <Input
                    className="vbctql__modal-input"
                    placeholder="Nhập tên nhóm"
                    value={addFolderInput}
                    onChange={handleChangeAddFolderInput}
                ></Input>
                <div className="vbctql__modal-actions">
                    <Button className="btn" onClick={handleCancel}>
                        Hủy
                    </Button>
                    <Button
                        className="btn btn_create"
                        type="primary"
                        onClick={handleOk}
                    >
                        Tạo nhóm
                    </Button>
                </div>
            </Modal>
        </div>
    )
}

export default Vbct_quanly
