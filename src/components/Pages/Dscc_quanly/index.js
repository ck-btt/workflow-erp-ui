import { useState } from "react"
import { Breadcrumb, Button, Table, Input } from "antd"
import { Link } from "react-router-dom"

import "./Dscc_quanly.less"
import SmallHeader from "../components/SmallHeader"
import AddButton from "../components/AddButton"
import { useSelector } from "react-redux"
import { getTemplateSelector } from "../../../redux/selectors"

function Dscc_quanly() {
    // const dispatch = useDispatch()

    // useEffect(() => {
    //     dispatch(getTemplates())
    // }, [dispatch])

    let locale = {
        emptyText:
            // isLoading ? (
            //     <Spin size="large"></Spin>
            // ) : (
            "Không có tài liệu nào.",
        // ),
    }

    const templateList = useSelector(getTemplateSelector)
    const [searchKey, setSearchKey] = useState("")
    const [tableData, setTableData] = useState(false)

    const handleInputSearch = (e) => {
        setSearchKey(e.target.value)
    }

    const handleSearch = () => {
        setTableData(
            templateList.filter((item) => {
                return item.desc.toLowerCase().includes(searchKey.toLowerCase())
            })
        )
    }

    const columns = [
        {
            key: "1",
            //   width: 900,
            title: (
                <div className="dsccql-table__title dsccql-table__first-title">
                    Nhóm
                </div>
            ),
            render: (item) => {
                return (
                    <div className="dsccql-table__content dsccql-table__first-content">
                        {item.folder_name}
                    </div>
                )
            },
        },
        {
            key: "2",
            //   width: 900,
            title: <div className="dsccql-table__title">Tên tài liệu</div>,
            render: (item) => {
                return <div className="dsccql-table__content">{item.desc}</div>
            },
        },

        {
            key: "4",
            title: "",
            render: (item) => {
                return (
                    <div className="dsccql-table__buttons">
                        <Link to="/taoqt" state={item.template_id}>
                            <Button className="dsccql-table__button-edit">
                                Chỉnh sửa
                            </Button>
                        </Link>
                        <Button className="dsccql-table__button-delete">
                            Xóa
                        </Button>
                    </div>
                )
            },
        },
    ]

    return (
        <div>
            <Breadcrumb className="breadcrumb" separator="»">
                <Breadcrumb.Item>Tài liệu</Breadcrumb.Item>
                <Breadcrumb.Item>Quy trình vận hành</Breadcrumb.Item>
            </Breadcrumb>
            <div className="dsccql-layout-background">
                <SmallHeader title="Quy trình vận hành">
                    <Link to="/taoqt">
                        <AddButton content="Tạo quy trình" />
                    </Link>
                </SmallHeader>

                <div className="dsccql-content">
                    {/* <SearchSection onclick={() => {}} /> */}
                    <div className="dsccql__search">
                        <Input
                            className="dsccql__search-input"
                            // style={{ paddingLeft: 19 }}
                            placeholder="Nhập tài liệu"
                            allowClear
                            value={searchKey}
                            onChange={handleInputSearch}
                        />
                        <Button
                            className="search-btn"
                            style={{
                                marginLeft: 39,
                                width: 151,
                                height: 44,
                                backgroundColor: "#3699ff",
                                color: "#ffffff",
                            }}
                            onClick={handleSearch}
                        >
                            Tìm kiếm
                        </Button>
                    </div>
                </div>

                <Table
                    className="dsccql-table"
                    columns={columns}
                    locale={locale}
                    dataSource={tableData || templateList}
                    // lấy trong dataSource cột 'id' để làm key cho từng row
                    rowKey={(item) => item.template_id}
                    rowClassName={"table-row"}
                    pagination={
                        templateList.length <= 10
                            ? false
                            : {
                                  pageSize: 10,
                              }
                    } // mỗi trang nhiều nhất 10 tasks
                ></Table>
            </div>
        </div>
    )
}

export default Dscc_quanly
