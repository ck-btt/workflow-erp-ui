import { Breadcrumb, Table, Button, Modal } from "antd"
import SmallHeader from "../components/SmallHeader"
import SearchSection from "../components/SearchSection"
import AddButton from "../components/AddButton"
import { Arr_Down, Arr_Up, Close } from "../../../assets/images"
import { useEffect, useState } from "react"
import axios from "axios"
import "./main.less"
import {
    getDocsSentByMeSelector,
    getTemplateForUserSelector,
} from "../../../redux/selectors"
import { useDispatch, useSelector } from "react-redux"
import { getTemplatesForUser } from "../../../redux/slices/templateSlice"
import { getDocsSentByMe } from "../../../redux/slices/documentSlice"

function Da_gui() {
    const dispatch = useDispatch()
    const [isModalVisible, setIsModalVisible] = useState(false)
    const [isFolder, setIsFolder] = useState(1)
    const templateList = useSelector(getTemplateForUserSelector)
    const listSentByMe = useSelector(getDocsSentByMeSelector)

    console.log("da gui", listSentByMe)

    let locale = {
        emptyText:
            // isLoading ? (
            //     <Spin size="large"></Spin>
            // ) : (
            "Không có tài liệu nào.",
        // ),
    }

    useEffect(() => {
        dispatch(getTemplatesForUser())
        dispatch(getDocsSentByMe())
    }, [dispatch])

    const folderIds = []
    const folderL = templateList.reduce((acc, template) => {
        if (!folderIds.includes(template.folder_id)) {
            folderIds.push(template.folder_id)
            return [
                ...acc,
                {
                    id: template.folder_id,
                    name: template.folder_name,
                },
            ]
        }
        return [...acc]
    }, [])

    const combineList = folderL.reduce((acc, folder) => {
        const templates = templateList.filter(
            (template) => template.folder_id === folder.id
        )
        return [...acc, { ...folder, templates }]
    }, [])

    const showModal = () => {
        setIsModalVisible(true)
    }

    const handleOk = () => {
        setIsModalVisible(false)
    }

    const handleCancel = () => {
        setIsModalVisible(false)
    }

    const handleSeeDocDetail = (id) => {
        let decodedCookies = decodeURIComponent(document.cookie)
        let cookies = decodedCookies.split("; ")
        let token
        for (const i of cookies) {
            if (i.startsWith("token")) {
                token = i.split("=")[1]
            }
        }
        axios
            .request({
                method: "get",
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                url: `${process.env.HD_EXP_DOMAIN}/me/doc/${id}/detail/`,
            })
            .then((response) => console.log("res", response.data))
            .catch((err) => console.log("er", err))
    }

    const columns = [
        {
            key: "1",
            width: 320,
            title: <div className="table_title ml51">Ngày tạo</div>,
            render: (item) => {
                return (
                    <div className="table_title ml51">
                        {item.create_date
                            .split("T")[0]
                            .split("-")
                            .reverse()
                            .join("/")}
                    </div>
                )
            },
        },
        {
            key: "2",
            width: 303,
            title: <div className="table_title">Tên văn bản</div>,
            render: (item) => {
                return <div className="fw700">{item.doc_title}</div>
            },
        },
        {
            key: "3",
            width: 299,
            title: <div className="table_title">Xử lý</div>,
            render: (item) => {
                let color = "#3699ff"
                if (item.status === "từ chối") {
                    color = "#f23f44"
                }
                return (
                    <div className="fw700">
                        {item.last_action_by.user_name !==
                        item.start_at.user_name
                            ? item.last_action_by.user_name
                            : "Chờ xử lý"}
                        <span style={{ color: color }}> {item.status}</span>
                        {item.last_action_by.user_name ===
                            item.start_at.user_name && (
                            <button className="remind_btn">--Nhắc nhở--</button>
                        )}
                    </div>
                )
            },
        },
        {
            key: "4",
            title: "",
            render: (item) => {
                return (
                    <div className="btn_section">
                        <Button
                            className="table_btn"
                            onClick={() => handleSeeDocDetail(item.doc_id)}
                        >
                            Xem
                        </Button>
                        <Button className="table_btn btn_reject">
                            Thu hồi
                        </Button>
                    </div>
                )
            },
        },
    ]

    return (
        <div>
            <Breadcrumb
                style={{
                    margin: "16px 0",
                    color: "#434349",
                }}
                separator="»"
            >
                <Breadcrumb.Item>Tài liệu</Breadcrumb.Item>
                <Breadcrumb.Item className="bread-active">
                    Đã gửi
                </Breadcrumb.Item>
            </Breadcrumb>
            <div className="dsvb_user_background">
                <SmallHeader title="Đã gửi">
                    <AddButton content="Tạo tài liệu" onclick={showModal} />
                    <Modal
                        // title="Basic Modal"
                        visible={isModalVisible}
                        onOk={handleOk}
                        onCancel={handleCancel}
                        centered={true}
                        className="dsvb_user_modal"
                        footer={null}
                        closeIcon={
                            <img src={Close} style={{ marginTop: -5 }} />
                        }
                    >
                        <div className="modal_header">
                            <h1>Tạo loại tài liệu</h1>
                        </div>
                        <div className="modal_body">
                            {combineList.map((folder) => (
                                <div className="modal_item" key={folder.id}>
                                    <div
                                        className="modal_folder"
                                        onClick={() => {
                                            folder.id !== isFolder
                                                ? setIsFolder(folder.id)
                                                : setIsFolder(0)
                                        }}
                                    >
                                        <span style={{ flex: 1 }}>
                                            {folder.name}
                                        </span>
                                        <img
                                            src={
                                                folder.id === isFolder
                                                    ? Arr_Up
                                                    : Arr_Down
                                            }
                                            alt=""
                                        />
                                    </div>
                                    {folder.id === isFolder && (
                                        <div className={`modal_content`}>
                                            {folder.id === isFolder &&
                                            folder.templates.length !== 0 ? (
                                                folder.templates.map(
                                                    (template, index) => (
                                                        <div
                                                            key={
                                                                template.template_id
                                                            }
                                                            className={`modal_template`}
                                                        >
                                                            {
                                                                template.template_name
                                                            }
                                                        </div>
                                                    )
                                                )
                                            ) : (
                                                <div
                                                    className={`modal_template_none`}
                                                >
                                                    Chưa có tài liệu
                                                </div>
                                            )}
                                        </div>
                                    )}
                                </div>
                            ))}
                        </div>
                    </Modal>
                </SmallHeader>

                {/* search */}
                <div className="content">
                    <SearchSection
                        onclick={() => {}}
                        placeholder="Tên văn bản"
                    />
                </div>

                <Table
                    className="dsvb_user_table"
                    columns={columns}
                    locale={locale}
                    dataSource={listSentByMe}
                    // lấy trong dataSource cột 'id' để làm key cho từng row
                    rowKey={(item) => item.doc_id}
                    rowClassName={"table-row"}
                    pagination={
                        listSentByMe.length <= 10
                            ? false
                            : {
                                  pageSize: 10,
                              }
                    } // mỗi trang nhiều nhất 10 tasks
                ></Table>
            </div>
        </div>
    )
}

export default Da_gui
