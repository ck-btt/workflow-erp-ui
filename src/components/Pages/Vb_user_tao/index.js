import $ from "jquery"
import { Breadcrumb, Button, Input, Upload, message } from "antd"
import "./main.less"
import SmallHeader from "../components/SmallHeader"
import { Upload_ic } from "../../../assets/images"
import { createRef, useEffect, useRef, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { getNewDocJsonDataSelector } from "../../../redux/selectors"
import { Link } from "react-router-dom"
import { submitTemplate } from "../../../redux/slices/documentSlice"

window.jQuery = $ // JQuery alias
window.$ = $

require("jquery-ui-sortable")
require("formBuilder")
require("formBuilder/dist/form-render.min.js")

function Vb_user_tao() {
    const dispatch = useDispatch()

    const [fileList, setFileList] = useState([])
    const titleRef = useRef()
    const fileInput = createRef()
    const fb = createRef()
    let formData, jsonData
    const newDocJson = useSelector(getNewDocJsonDataSelector)

    useEffect(() => {
        const config = {
            formData: newDocJson.jsondata,
        }
        formData = $(fb.current).formRender(config)
    }, [])

    console.log(fileList)

    const steps = [
        {
            user: "Phạm Việt Đông Hải",
            create_date: "02/01/2022 11:18:08",
            isActive: true,
        },
        { user: "Phan Nghệ Đô", isActive: false },
        { user: "Nguyễn Thị Lệ", isActive: false },
    ]

    const props = {
        name: "file",
        action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
        headers: {
            authorization: "authorization-text",
        },

        onChange(info) {
            if (info.file.status !== "uploading") {
                console.log(info.file, info.fileList)
            }

            if (info.file.status === "done") {
                message.success(`${info.file.name} file uploaded successfully`)
            } else if (info.file.status === "error") {
                message.error(`${info.file.name} file upload failed.`)
            }
        },
    }

    const handleSubmit = () => {
        jsonData = formData.userData
        const data = {
            template_id: newDocJson.template_id,
            title: titleRef.current.input.value,
            jsondata: jsonData,
        }

        console.log(JSON.stringify(jsonData))
        dispatch(submitTemplate(data))

        console.log(data)
    }

    const handleSubmitFile = (e) => {
        console.log(fileList)
    }

    const handleDeleteFile = (name) => {
        setFileList((prev) => prev.filter((file) => file.name !== name))
    }

    return (
        <div>
            <Breadcrumb
                style={{
                    margin: "16px 0",
                }}
                separator="»"
                className="breadcrumb"
            >
                <Breadcrumb.Item>Tài liệu</Breadcrumb.Item>
                <Breadcrumb.Item>
                    <Link to={"/u"}>Tạo mới</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item className="bread-active">
                    {newDocJson.template_name}
                </Breadcrumb.Item>
            </Breadcrumb>
            <div className="vb_user_t_background">
                <SmallHeader title={newDocJson.template_name}></SmallHeader>
                <div className="vb_user_t_title-section">
                    <h2>Tiêu đề Văn bản</h2>
                    <Input className="vb_user_t_title-input" ref={titleRef} />
                </div>
                <div className="fb-editor" ref={fb}></div>

                <div className="vb_user_t_container">
                    <div className="input_section">
                        {/* <Upload {...props}>
                            <Button className="file_btn" style={{ height: 44 }}>
                                <img src={Upload_ic} alt="" />
                                Chọn tài liệu đính kèm
                            </Button>
                        </Upload> */}

                        <label className="vb_user_t_input-file-btn">
                            Them tep dinh kem:
                            <input
                                type="file"
                                ref={fileInput}
                                multiple="multiple"
                                onChange={(e) => {
                                    setFileList((prev) => [
                                        ...prev,
                                        fileInput.current.files[0],
                                    ])
                                    console.log("path: ", e.target.value)
                                }}
                            />
                        </label>

                        {fileList.map((file, index) => (
                            <div
                                className="vb_user_t_file-section"
                                key={index}
                                style={{
                                    display: "flex",
                                    justifyContent: "space-between",
                                }}
                            >
                                <span className="vb_user_t_file-section-title">
                                    {file.name}
                                </span>
                                <span
                                    className="vb_user_t_file-section-clear"
                                    onClick={() => handleDeleteFile(file.name)}
                                >
                                    x
                                </span>
                            </div>
                        ))}
                        {/* <button onClick={handleSubmitFile}>Submit</button> */}

                        <div className="btn-section">
                            <Button className="btn">Xóa</Button>
                            <Button className="btn btn_save">Lưu</Button>
                            <Button
                                className="btn btn_create"
                                onClick={handleSubmit}
                            >
                                Tạo
                            </Button>
                        </div>
                    </div>
                    <div className="progress_section"></div>
                </div>
            </div>
        </div>
    )
}

export default Vb_user_tao
