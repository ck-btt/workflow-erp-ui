import { Button, Input } from "antd"
import { useNavigate } from "react-router-dom"
import {useRef} from 'react'
import { useDispatch } from 'react-redux'
import { setToken } from "../../../redux/slices/globalVariableSlice"

function Dang_nhap() {

    const dispatch = useDispatch()
    const navigate = useNavigate()
    const inputRef = useRef()
    const handleOnClick = () => {
        // write cookie
        document.cookie = `token=${inputRef.current.input.value}`

        // get cookie
        let decodedCookies = decodeURIComponent(document.cookie)
        let cookies = decodedCookies.split('; ');
        let token
        for (const i of cookies) {
            if (i.startsWith('token')) {
                token = i.split('=')[1]
            }
        }
        console.log('token', token);
        dispatch(setToken(token))
        navigate("/")
        window.location.reload()
    }
    return (
        <div
            style={{
                margin: "auto",
                textAlign: "center",
            }}
        >
            <h1>Đăng nhập đi ba</h1>
            <Input ref={inputRef}/>
            <br />{" "}
            <Button type="primary" onClick={handleOnClick}>
                Đăng nhập
            </Button>
        </div>
    )
}

export default Dang_nhap
