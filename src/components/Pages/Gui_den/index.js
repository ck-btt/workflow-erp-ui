import { Breadcrumb, Table, Button } from "antd"
import { useDispatch, useSelector } from "react-redux"
import { useEffect} from "react"

import "./main.less"
import SearchSection from "../components/SearchSection"
import { getDocsSentToMe } from "../../../redux/slices/documentSlice"
import { getDocsSentToMeSelector } from "../../../redux/selectors"
import axios from "axios"

function Gui_den() {

    const dispatch = useDispatch()
    const listSentToMe = useSelector(getDocsSentToMeSelector)

    console.log('gui den', listSentToMe)

    let locale = {
        emptyText: 
        // isLoading ? (
        //     <Spin size="large"></Spin>
        // ) : (
            "Không có tài liệu nào."
        // ),
    }
    
    useEffect(() => {
        dispatch(getDocsSentToMe())
    },[dispatch])

    const columns = [
        {
            key: "1",
            width: 310,
            title: <div className="table_title ml51">Người tạo</div>,
            render: (item) => {
                return <div className="fw700 ml51">{item.start_at.user_name}</div>
            },
        },
        {
            key: "2",
            width: 300,
            title: <div className="table_title">Loại văn bản</div>,
            render: (item) => {
                return <div className="table_content">{item.doc_title}</div>
            },
        },
        {
            key: "3",
            width: 320,

            title: <div className="table_title">Xử lý</div>,
            render: (item) => {
                let color = "#3699ff"
                if (item.status === "Đã duyệt") color = "#35794a"
                else if (item.status === "Chưa xem") color = "#f23f44"
                return (
                    <div className="table_content" style={{ color: color }}>
                        {item.last_action_by.action_name}
                    </div>
                )
            },
        },
        {
            key: "4",
            width: 216,
            title: <div className="table_title">Ngày tạo</div>,
            render: (item) => {
                return <div className="table_title">
                        {item.create_date.split('T')[0].split('-').reverse().join('/')}

                </div>
            },
        },

        {
            key: "5",
            title: "",
            render: (item) => {
                return (
                    <div className="btn_section">
                        <Button 
                            className="table_btn"
                            onClick={() =>handleGetDocDetail(item.doc_id)}
                        >
                            Xem
                        </Button>
                    </div>
                )
            },
        },
    ]

    const handleGetDocDetail = (id) => {
        let decodedCookies = decodeURIComponent(document.cookie)
        let cookies = decodedCookies.split('; ');
        let token
        for (const i of cookies) {
            if (i.startsWith('token')) {
                token = i.split('=')[1]
            }
        }
        axios.request({
            method: "get",
            headers: {
                Authorization: `Bearer ${token}`
            },
            url: `${process.env.HD_EXP_DOMAIN}/me/doc/${id}/detail/` ,
        })
        .then(response => console.log('res',response.data))
        .catch(err => console.log('er', err))
    }

    return (
        <div>
            <Breadcrumb
                style={{
                    margin: "16px 0",
                    color: "#434349",
                }}
                separator="»"
            >
                <Breadcrumb.Item>Tài liệu</Breadcrumb.Item>
                <Breadcrumb.Item className="bread-active">
                    Gửi đến
                </Breadcrumb.Item>
            </Breadcrumb>
            <div className="dsvb_user_d_background">
                {/* <SmallHeader title="Tài liệu của tôi"></SmallHeader> */}

                <div className="dsvb_user_content">
                    <SearchSection
                        onclick={() => {}}
                        placeholder="Tên văn bản"
                    />
                </div>

                <Table
                    className="dsvb_user_table"
                    columns={columns}
                    locale={locale}
                    dataSource={listSentToMe}
                    // lấy trong dataSource cột 'id' để làm key cho từng row
                    rowKey={(item) => item.doc_id}
                    rowClassName={"table-row"}
                    pagination={
                        listSentToMe.length <= 10
                            ? false
                            : {
                                  pageSize: 10,
                              }
                    } // mỗi trang nhiều nhất 10 tasks
                ></Table>
            </div>
        </div>
    )
}

export default Gui_den
