import { Breadcrumb, DatePicker, Input } from "antd"
import SmallHeader from "../components/SmallHeader"
import moment from "moment"
import TextArea from "antd/lib/input/TextArea"
import "./main.less"

function Vb_user_duyet() {
    const steps = [
        {
            user: "Phạm Việt Đông Hải",
            create_date: "02/01/2022 11:18:08",
            file: "dexuat.doc",
            isActive: true,
        },
        { user: "Phan Nghệ Đô", status: "Đã duyệt", isActive: true },
        { user: "Nguyễn Thị Lệ", isActive: false },
    ]
    return (
        <div>
            <Breadcrumb
                style={{
                    margin: "16px 0",
                    color: "#434349",
                }}
                separator="»"
            >
                <Breadcrumb.Item>Tài liệu</Breadcrumb.Item>
                <Breadcrumb.Item className="bread-active">
                    Đơn xin nghỉ phép
                </Breadcrumb.Item>
            </Breadcrumb>
            <div className="vb_user_duyet_background">
                <SmallHeader title="Đơn xin nghỉ phép"></SmallHeader>
                <div className="vb_user_d_container">
                    <div className="input_section">
                        <Input
                            className="input input_disabled"
                            value="Phạm Việt Đông Hải"
                        />
                        <Input
                            className="input input_disabled"
                            value="Phòng CNTT"
                        />
                        <Input className="input input_disabled" value="1" />
                        <div className="date_picker">
                            <DatePicker
                                className="input_disabled"
                                placeholder="Từ ngày"
                                format={"DD/MM/YYYY"}
                                defaultValue={moment(
                                    "21/08/2022",
                                    "DD/MM/YYYY"
                                )}
                                style={{
                                    backgroundColor: "#F0F5FA",
                                    border: "none",
                                    height: 44,
                                }}
                            />
                            <DatePicker
                                className="input_disabled"
                                placeholder="Đến ngày"
                                format={"DD/MM/YYYY"}
                                defaultValue={moment(
                                    "22/08/2022",
                                    "DD/MM/YYYY"
                                )}
                                style={{
                                    backgroundColor: "#F0F5FA",
                                    border: "none",
                                    height: 44,
                                }}
                            />
                        </div>
                        <TextArea
                            className="input medium_input input_disabled"
                            placeholder="Lý do nghỉ"
                            value="Việc gia đình"
                            autoSize={{ minRows: 3, maxRows: 5 }}
                        />
                        <TextArea
                            className="input medium_input input_disabled"
                            placeholder="Địa điểm nghỉ"
                            value="TP.HCM quận Bình Thạnh"
                            autoSize={{ minRows: 3, maxRows: 5 }}
                        />
                    </div>
                    <div className="progress_section">
                        {steps.map((step, index) => (
                            <div
                                className={`step ${
                                    step.isActive && "step_active"
                                }`}
                                key={index}
                                style={
                                    !step.isActive ? { display: "none" } : {}
                                }
                            >
                                <div className={"step_num"}>{index + 1}</div>
                                <div className="step_info">
                                    <div className="step_user">{step.user}</div>
                                    <div className="step_date">
                                        {step.create_date
                                            ? "Đã tạo: " + step.create_date
                                            : ""}
                                    </div>
                                    <div className="step_status">
                                        {step.status ? step.status : ""}
                                    </div>
                                    {step.file && (
                                        <div className="step_file">
                                            {step.file}
                                        </div>
                                    )}
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Vb_user_duyet
