import { useEffect, useState, useRef } from "react"
import { useDispatch, useSelector } from "react-redux"
import {
    Button,
    Input,
    Modal,
    Checkbox,
    InputNumber,
    Tabs,
    Select,
    message,
} from "antd"
import axios from "axios"

import "./ModalCreateStep.less"
import { Close, Search_ic } from "../../../../assets/images"
import { toggleModalCreateStepVisible } from "../../../../redux/slices/modalVisibleSlice"
import {
    getPartListSelector,
    getPositionListSelector,
    getUserListSelector,
} from "../../../../redux/selectors"
import {
    getParts,
    getPositions,
    getUsers,
} from "../../../../redux/slices/fakeDataSlice"
import { toggleCreateStepSucceed } from "../../../../redux/slices/refreshVariableSlice"

let timeout, currentValue

function ModalCreateStep({ visible, template_id }) {
    const dispatch = useDispatch()
    const [stepInputName, setStepInputName] = useState([])
    const [stepInputPart, setStepInputPart] = useState([])
    const [stepInputPosition, setStepInputPosition] = useState([])
    const [currentTab, setCurrentTab] = useState(1)
    const [checkboxValue, setCheckboxValue] = useState(false)
    const stepNumberRef = useRef()
    const { Option } = Select

    const partList = useSelector(getPartListSelector)
    const positionList = useSelector(getPositionListSelector)
    const userList = useSelector(getUserListSelector)

    const tabPanels = [
        {
            id: 1,
            tabName: "Tên",
            // placeholder: "Nhập tên",
            // value: stepInputName,
        },
        {
            id: 2,
            tabName: "Phòng ban",
            // placeholder: "Nhập phòng ban",
            // value: stepInputPart,
        },
        {
            id: 3,
            tabName: "Vị trí",
            // placeholder: "Nhập vị trí",
            // value: stepInputPosition,
        },
    ]

    const handleChangeTab = (key) => {
        setCurrentTab(key)
    }

    const handleCheckbox = (e) => {
        setCheckboxValue(e.target.checked)
    }

    const handleCreateStep = () => {
        // console.log("name", stepInputName)
        // console.log("part", stepInputPart)
        // console.log("position", stepInputPosition)
        // console.log("current tab", currentTab)
        // console.log('checkbox', checkboxValue);
        // console.log("step number", stepNumberRef.current.value)
        // console.log("tid", template_id)

        if (!stepNumberRef.current.value) {
            message.warn("Hãy nhập step number")
        } else {
            if (currentTab == 1 && stepInputName.length === 0) {
                message.warn("Hãy nhập tên người")
            } else if (currentTab == 2 && stepInputPart.length === 0) {
                message.warn("Hãy nhập tên phòng ban")
            } else if (currentTab == 3 && stepInputPosition.length === 0) {
                message.warn("Hãy nhập tên vị trí")
            } else {
                // let objExecuteList
                // currentTab == 1
                //     ? (objExecuteList = userList.map((user, index) => {
                //           for (const i of stepInputName) {
                //               if (user.name == i) {
                //                   return user.id
                //               }
                //           }
                //       }))
                //     : currentTab == 2
                //     ? (objExecuteList = partList.map((part, index) => {
                //           for (const i of stepInputPart) {
                //               if (part.name == i) {
                //                   return part.id
                //               }
                //           }
                //       }))
                //     : (objExecuteList = positionList.map((position, index) => {
                //           for (const i of stepInputPosition) {
                //               if (position.name == i) {
                //                   return position.id
                //               }
                //           }
                //       }))

                // let objExecute = {}
                // if (currentTab == 1) {
                //     objExecute["user_id"] = objExecuteList.filter(
                //         (item) => item
                //     )
                // } else if (currentTab == 2) {
                //     objExecute["part_id"] = objExecuteList.filter(
                //         (item) => item
                //     )
                // } else {
                //     objExecute["position_id"] = objExecuteList.filter(
                //         (item) => item
                //     )
                // }

                const idList = (
                    currentTab == 1
                        ? stepInputName
                        : currentTab == 2
                        ? stepInputPart
                        : stepInputPosition
                ).map((step) => step.id)

                const apiRequestBody = {
                    template_ID: template_id,
                    is_start: checkboxValue,
                    step_number: stepNumberRef.current.value,
                    // ...objExecute,
                }
                const key =
                    currentTab == 1
                        ? "user_id"
                        : currentTab == 2
                        ? "part_id"
                        : "position_id"
                apiRequestBody[key] = idList

                axios
                    .post(
                        `${process.env.HD_EXP_DOMAIN}/step/create`,
                        apiRequestBody
                    )
                    .then((response) => {
                        // console.log("res", response)
                        message.success(response.data.data)
                        setCheckboxValue(false)
                        setStepInputName([])
                        setStepInputPart([])
                        setStepInputPosition([])
                        dispatch(toggleModalCreateStepVisible())
                        dispatch(toggleCreateStepSucceed())
                    })
                    .catch((e) => {
                        if (e.response.data.detail.result == false) {
                            message.error(e.response.data.detail.data)
                        } else {
                            message.error(e.response.data.detail)
                        }
                        // console.log("err", e)
                    })
            }
        }
    }

    // Tùy theo tab mà chọn link request khác nhau
    const fetch = (value, callback) => {
        if (timeout) {
            clearTimeout(timeout)
            timeout = null
        }

        currentValue = value

        const fake = () => {
            if (currentTab == 1) {
                axios
                    .request({
                        method: "get",
                        url:
                            process.env.HD_EXP_DOMAIN +
                            "/sampledata/user/list" +
                            `?kw=${value}`,
                    })
                    .then((res) => callback(res.data.message))
            }
            if (currentTab == 2) {
                axios
                    .request({
                        method: "get",
                        url:
                            process.env.HD_EXP_DOMAIN +
                            "/sampledata/part/list" +
                            `?kw=${value}`,
                    })
                    .then((res) => callback(res.data.message))
            }
            if (currentTab == 3) {
                axios
                    .request({
                        method: "get",
                        url:
                            process.env.HD_EXP_DOMAIN +
                            "/sampledata/position/list" +
                            `?kw=${value}`,
                    })
                    .then((res) => callback(res.data.message))
            }
        }

        timeout = setTimeout(fake, 1000)
    }

    const SearchInput = (props) => {
        const [data, setData] = useState([])
        const [value, setValue] = useState()

        const handleSearch = (newValue) => {
            if (newValue) {
                fetch(newValue, setData)
            } else {
                setData([])
            }
        }

        const handleChange = (newValue) => {
            setValue(newValue)
            currentTab == 1
                ? setStepInputName((prev) =>
                      prev.find((value) => value.id == newValue)
                          ? [...prev]
                          : [...prev, data.find((d) => d.id == newValue)]
                  )
                : currentTab == 2
                ? setStepInputPart((prev) =>
                      prev.find((value) => value.id == newValue)
                          ? [...prev]
                          : [...prev, data.find((d) => d.id == newValue)]
                  )
                : setStepInputPosition((prev) =>
                      prev.find((value) => value.id == newValue)
                          ? [...prev]
                          : [...prev, data.find((d) => d.id == newValue)]
                  )
        }

        const options = data.map((d) => <Option key={d.id}>{d.name}</Option>)
        return (
            <Select
                showSearch
                value={value}
                placeholder={props.placeholder}
                style={props.style}
                defaultActiveFirstOption={false}
                showArrow={false}
                filterOption={false}
                onSearch={handleSearch}
                onChange={handleChange}
                notFoundContent={null}
                className="tqt__search-input"
                allowClear
            >
                {options}
            </Select>
        )
    }

    const handleClearData = (id) => {
        console.log(id)

        currentTab == 1
            ? setStepInputName((prev) =>
                  prev.filter((value) => value.id !== id)
              )
            : currentTab == 2
            ? setStepInputPart((prev) =>
                  prev.filter((value) => value.id !== id)
              )
            : setStepInputPosition((prev) =>
                  prev.filter((value) => value.id !== id)
              )
    }


    return (
        <Modal
            visible={visible}
            onOk={handleCreateStep}
            onCancel={() => {
                dispatch(toggleModalCreateStepVisible())
            }}
            centered={true}
            className="tqt__modal"
            footer={null}
            closeIcon={<img src={Close} />}
        >
            <h3 className="tqt__modal-title">Thêm quy trình</h3>

            <div className="tqt__modal-body">
                <Tabs
                    defaultActiveKey="1"
                    onChange={handleChangeTab}
                    className="tqt__modal-tabs"
                >
                    {tabPanels.map((item) => {
                        return (
                            <Tabs.TabPane tab={item.tabName} key={item.id}>
                                {/* <div className="tqt__modal-search-icon">
                                    <img src={Search_ic} />
                                </div> */}
                                <SearchInput placeholder="Input search text" />
                                <div className="tqt__modal-list">
                                    {(currentTab == 1
                                        ? stepInputName
                                        : currentTab == 2
                                        ? stepInputPart
                                        : stepInputPosition
                                    ).map((step) => (
                                        <div
                                            key={step.id}
                                            className="tqt__modal-item"
                                        >
                                            <div className="tqt__modal-item-name">
                                                {step.name}
                                            </div>
                                            <div
                                                className="tqt__modal-item-clear"
                                                onClick={() =>
                                                    handleClearData(step.id)
                                                }
                                            >
                                                Clear
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </Tabs.TabPane>
                        )
                    })}

                    <Tabs.TabPane tab="Phòng ban/Vị trí" key="4">
                        Content of Tab Pane 4
                    </Tabs.TabPane>
                </Tabs>
            </div>

            <div className="tqt__modal-footer">
                <div className="tqt__modal-step-info">
                    <div className="tqt__modal-step-number">
                        <span>Số thứ tự: </span>
                        <InputNumber
                            size="medium"
                            min={1}
                            className="tqt__modal-step-number-input"
                            placeholder="..."
                            ref={stepNumberRef}
                        />
                    </div>
                    <Checkbox onChange={handleCheckbox}>Bước bắt đầu</Checkbox>
                </div>
                <Button
                    className="btn btn_create"
                    type="primary"
                    onClick={handleCreateStep}
                >
                    Hoàn thành
                </Button>
            </div>
        </Modal>
    )
}

export default ModalCreateStep
