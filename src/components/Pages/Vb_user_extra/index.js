import { Breadcrumb, Button, DatePicker, Input, Select, Upload } from "antd"
import SmallHeader from "../components/SmallHeader"
import moment from "moment"
import TextArea from "antd/lib/input/TextArea"
import "./main.less"
import { Arr_Down, Upload_ic } from "../../../assets/images"
const { Option } = Select

function Vb_user_extra() {
    const steps = [
        {
            user: "Phạm Việt Đông Hải",
            create_date: "02/01/2022 11:18:08",
            file: "dexuat.doc",
            isActive: true,
        },
        { user: "Phan Nghệ Đô", status: "Đã duyệt", isActive: true },
        { user: "Nguyễn Thị Lệ", isActive: false },
    ]
    const props = {
        name: "file",
        action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
        headers: {
            authorization: "authorization-text",
        },

        onChange(info) {
            if (info.file.status !== "uploading") {
                console.log(info.file, info.fileList)
            }

            if (info.file.status === "done") {
                message.success(`${info.file.name} file uploaded successfully`)
            } else if (info.file.status === "error") {
                message.error(`${info.file.name} file upload failed.`)
            }
        },
    }
    return (
        <div>
            <Breadcrumb
                style={{
                    margin: "16px 0",
                    color: "#434349",
                }}
                separator="»"
            >
                <Breadcrumb.Item>Tài liệu</Breadcrumb.Item>
                <Breadcrumb.Item className="bread-active">
                    Đơn xin nghỉ phép
                </Breadcrumb.Item>
            </Breadcrumb>
            <div className="vb_user_duyet_background">
                <SmallHeader title="Đơn xin nghỉ phép"></SmallHeader>
                <div className="vb_user_d_container">
                    <div className="input_section">
                        <Input
                            className="input input_disabled"
                            value="Phạm Việt Đông Hải"
                        />
                        <Input
                            className="input input_disabled"
                            value="Phòng CNTT"
                        />
                        <Input className="input input_disabled" value="1" />
                        <div className="date_picker">
                            <DatePicker
                                className="input_disabled"
                                placeholder="Từ ngày"
                                format={"DD/MM/YYYY"}
                                defaultValue={moment(
                                    "21/08/2022",
                                    "DD/MM/YYYY"
                                )}
                                style={{
                                    backgroundColor: "#F0F5FA",
                                    border: "none",
                                    height: 44,
                                }}
                            />
                            <DatePicker
                                className="input_disabled"
                                placeholder="Đến ngày"
                                format={"DD/MM/YYYY"}
                                defaultValue={moment(
                                    "22/08/2022",
                                    "DD/MM/YYYY"
                                )}
                                style={{
                                    backgroundColor: "#F0F5FA",
                                    border: "none",
                                    height: 44,
                                }}
                            />
                        </div>
                        <TextArea
                            className="input medium_input input_disabled"
                            placeholder="Lý do nghỉ"
                            value="Việc gia đình"
                            autoSize={{ minRows: 3, maxRows: 5 }}
                        />
                        <TextArea
                            className="input medium_input input_disabled"
                            placeholder="Địa điểm nghỉ"
                            value="TP.HCM quận Bình Thạnh"
                            autoSize={{ minRows: 3, maxRows: 5 }}
                        />
                    </div>
                    <div className="progress_section">
                        {steps.map((step, index) => (
                            <div
                                className={`step ${
                                    step.isActive && "step_active"
                                }`}
                                style={
                                    !step.isActive ? { display: "none" } : {}
                                }
                                key={index}
                            >
                                <div className={"step_num"}>{index + 1}</div>
                                <div className="step_info">
                                    <div className="step_user">{step.user}</div>
                                    <div className="step_date">
                                        {step.create_date
                                            ? "Đã tạo: " + step.create_date
                                            : ""}
                                    </div>
                                    <div className="step_status">
                                        {step.status ? step.status : ""}
                                    </div>
                                    {step.file && (
                                        <div className="step_file">
                                            {step.file}
                                        </div>
                                    )}
                                </div>
                            </div>
                        ))}
                        <div className="extra_section">
                            <Upload {...props}>
                                <Button
                                    className="extra_file"
                                    style={{ height: 44 }}
                                >
                                    <img src={Upload_ic} alt="" />
                                    Chọn tài liệu đính kèm
                                </Button>
                            </Upload>

                            <div className="extra_star">
                                <TextArea
                                    className="extra_note"
                                    placeholder="Ghi chú"
                                    // value="Việc gia đình"
                                    autoSize={{ minRows: 3, maxRows: 5 }}
                                ></TextArea>
                            </div>
                            <Select
                                className="extra_action"
                                placeholder="Chọn thao tác"
                                style={{
                                    height: 44,
                                }}
                                listHeight={150}
                                suffixIcon={
                                    <img
                                        className="extra_action_icon"
                                        src={Arr_Down}
                                    ></img>
                                }
                                // onChange={handleChange}
                            >
                                <Option value="jack">Jack</Option>
                                <Option value="lucy">Lucy</Option>
                                <Option value="Yiminghe">
                                    <span onClick={() => console.log("hi")}>
                                        yiminghe
                                    </span>
                                </Option>
                            </Select>
                            <Input
                                className="input extra_note"
                                placeholder="Tham khảo"
                            />
                            <Button className="extra_btn">Hoàn thành</Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Vb_user_extra
