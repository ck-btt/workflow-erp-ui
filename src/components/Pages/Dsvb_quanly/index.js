import { Link, useNavigate } from "react-router-dom"

import { useState, useEffect } from "react"

import { Breadcrumb, Button, Table, Spin, Input } from "antd"
import "./Dsvb_quanly.less"
import SmallHeader from "../components/SmallHeader"
import AddButton from "../components/AddButton"
import { useDispatch, useSelector } from "react-redux"
import { getTemplateSelector } from "../../../redux/selectors"
import { getTemplates } from "../../../redux/slices/templateSlice"
import { setEditingTemplate } from "../../../redux/slices/globalVariableSlice"

function Dsvb_quanly() {
    useEffect(() => {
        dispatch(getTemplates())
    }, [dispatch])

    const navigate = useNavigate()
    const dispatch = useDispatch()
    const templateList = useSelector(getTemplateSelector)
    const [searchKey, setSearchKey] = useState("")
    const [tableData, setTableData] = useState(false)

    let locale = {
        emptyText: "Không có tài liệu nào.",
    }

    const handleInputSearch = (e) => {
        setSearchKey(e.target.value)
    }

    const handleSearch = () => {
        setTableData(
            templateList.filter((item) => {
                return item.desc.toLowerCase().includes(searchKey.toLowerCase())
            })
        )
    }

    const handleSelectTemplate = (item) => {
        dispatch(setEditingTemplate(item))
        navigate("/edit_template")
    }

    const columns = [
        {
            key: "1",
            width: 900,
            title: <div className="dsvbql-table__title">Tên tài liệu</div>,
            render: (item) => {
                return <div className="dsvbql-table__content">{item.desc}</div>
            },
        },
        {
            key: "2",
            title: "",
            render: (item) => {
                return (
                    <div className="dsvbql-table__buttons">
                        <Button
                            className="dsvbql-table__button-edit"
                            onClick={() => handleSelectTemplate(item)}
                        >
                            Chỉnh sửa
                        </Button>
                        <Button className="dsvbql-table__button-delete">
                            Xóa
                        </Button>
                    </div>
                )
            },
        },
    ]

    return (
        <div>
            <Breadcrumb className="breadcrumb" separator="»">
                <Breadcrumb.Item>Tài liệu</Breadcrumb.Item>
                <Breadcrumb.Item>Danh sách tài liệu</Breadcrumb.Item>
            </Breadcrumb>
            <div className="dsvbql-layout-background">
                <SmallHeader title="Danh sách tài liệu">
                    <Link to="/vbct">
                        <AddButton content="Tạo tài liệu" />
                    </Link>
                </SmallHeader>

                <div className="dsvbql-content">
                    {/* <SearchSection onclick={() => {}} /> */}
                    <div className="dsvbql__search">
                        <Input
                            className="dsvbql__search-input"
                            // style={{ paddingLeft: 19 }}
                            placeholder="Nhập tài liệu"
                            allowClear
                            value={searchKey}
                            onChange={handleInputSearch}
                        />
                        <Button
                            className="search-btn"
                            style={{
                                marginLeft: 39,
                                width: 151,
                                height: 44,
                                backgroundColor: "#3699ff",
                                color: "#ffffff",
                            }}
                            onClick={handleSearch}
                        >
                            Tìm kiếm
                        </Button>
                    </div>
                </div>
                <Table
                    className="dsvbql-table"
                    columns={columns}
                    locale={locale}
                    dataSource={tableData || templateList}
                    // lấy trong dataSource cột 'id' để làm key cho từng row
                    rowKey={(item) => item.template_id}
                    rowClassName={"table-row"}
                    pagination={
                        templateList?.length <= 10
                            ? false
                            : {
                                  pageSize: 10,
                              }
                    } // mỗi trang nhiều nhất 10 tasks
                ></Table>
            </div>
        </div>
    )
}

export default Dsvb_quanly
