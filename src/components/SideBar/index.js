import React from "react"
import { Layout, Menu } from "antd"
import { useState } from "react"
import { Link } from "react-router-dom"
import {
    Logo,
    Paper,
    Paper_Negative,
    Star,
    User,
    Work,
} from "../../assets/images"
import "./Sidebar.less"
const { Sider } = Layout

function Sidebar() {
    const [collapsed, setCollapsed] = useState(false)

    function getItem(label, key, icon, children) {
        return {
            key,
            icon,
            children,
            label,
            className: key,
        }
    }
    const items = [
        getItem(
            "Công việc của tôi",
            "menu-item-1",
            <img alt="" className="icon" src={Work}></img>
        ),
        getItem(
            "Văn bản",
            "menu-item-2",
            <img alt="" className="icon second-icon" src={Paper}></img>,
            [
                getItem(<Link to="/u">Tạo mới</Link>, "menu-item-9"),
                getItem(<Link to="/dagui">Đã gửi</Link>, "menu-item-10"),
                getItem(<Link to="/guiden">Gửi đến</Link>, "menu-item-11"),
            ]
        ),
        getItem(
            "Nhân sự",
            "menu-item-3",
            <img alt="" className="icon" src={User}></img>
        ),
        // Sub menu
        // getItem("User", "sub1", <UserOutlined />, [
        //     getItem("Tom", "3"),
        //     getItem("Bill", "4"),
        //     getItem("Alex", "5"),
        // ]),
        getItem(
            "Tài liệu",
            "menu-item-4",
            <img alt="" className="icon" src={Paper_Negative}></img>,
            [
                getItem(<Link to="/">Danh sách tài liệu</Link>, "menu-item-7"),
                getItem(
                    <Link to="/dscc">Quy trình vận hành</Link>,
                    "menu-item-8"
                ),
            ]
        ),
        getItem(
            "Đánh giá lao động",
            "menu-item-5",
            <img alt="" className="icon" src={Star}></img>
        ),
        // getItem("Tự đánh giá", "menu-item-6", <div className="last-icon"></div>),
    ]

    return (
        <>
            <Sider
                // collapsible

                width="265px"
                collapsed={collapsed}
                onCollapse={(value) => setCollapsed(value)}
            >
                <div className="logo">
                    <img src={Logo} alt="" />
                </div>
                <Menu
                    theme="dark"
                    defaultSelectedKeys={["1"]}
                    mode="inline"
                    items={items}
                />
            </Sider>
        </>
    )
}

export default Sidebar
