import { createSlice } from "@reduxjs/toolkit"

const folderSlice = createSlice({
    name: "folder",
    initialState: [],
    reducers: {
        getFolder() {},
        setFolder(state, action) {
            state.push(...action.payload)
        },
        createFolder() {},
    },
})

export const { getFolder, setFolder, createFolder } = folderSlice.actions
export default folderSlice
