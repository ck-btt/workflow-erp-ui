import { createSlice } from "@reduxjs/toolkit"

const globalVariableSlice = createSlice({
    name: "globalVariable",
    initialState: {
        token: "",
        newDocJsonData: {},
        editingTemplate: {}
    },
    reducers: {
        setToken(state, action) {
            state.token = action.payload
        },
        setNewDocJsonData(state, action) {
            state.newDocJsonData = action.payload
        },
        setEditingTemplate(state, action) {
            state.editingTemplate = action.payload
        },
    },
})

export const { setToken, setNewDocJsonData, setEditingTemplate } = globalVariableSlice.actions

export default globalVariableSlice
