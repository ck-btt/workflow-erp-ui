import { createSlice } from "@reduxjs/toolkit"

const documentSlice = createSlice({
    name: "document",
    initialState: {
        docsSentByMe: [],
        docsSentToMe: [],
    },
    reducers: {
        getDocsSentByMe() {},
        setDocsSentByMe(state, action) {
            state.docsSentByMe = action.payload
        },
        getDocsSentToMe() {},
        setDocsSentToMe(state, action) {
            state.docsSentToMe = action.payload
        },
        submitTemplate() {},
    },
})

export const {
    getDocsSentByMe,
    getDocsSentToMe,
    setDocsSentToMe,
    setDocsSentByMe,
    submitTemplate,
} = documentSlice.actions

export default documentSlice
