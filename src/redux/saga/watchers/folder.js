import { message } from "antd"
import { call, put, takeLatest, fork } from "redux-saga/effects"
import { setFolder, getFolder, createFolder } from "../../slices/folderSlice"
import { requestGetFolder, requestPostFolder } from "../requests/folder"

export function* handleGetFolder(action) {
    try {
        const response = yield call(requestGetFolder)
        const { data } = response
        yield put(setFolder(data.message))
    } catch (error) {
        console.log(error)
    }
}

function* onHandleGetFolder() {
    yield takeLatest(getFolder.type, handleGetFolder)
}

function* handleCreateFolder(action) {
    try {
        const response = yield call(requestPostFolder, action.payload)
        if (response.status === 200) {
            message.success("Tao thanh cong")
            const response = yield call(requestGetFolder)
            const { data } = response
            yield put(setFolder(data.message))
        }
    } catch (error) {
        console.log(error)
    }
}

function* onHandleCreateFolder() {
    yield takeLatest(createFolder.type, handleCreateFolder)
}

export const folderSaga = [fork(onHandleGetFolder), fork(onHandleCreateFolder)]
