import { call, put, takeLatest, fork } from "redux-saga/effects"
import {
    setTemplates,
    getTemplates,
    getTemplatesForUser,
    setTemplatesForUser,
    setTemplateById,
    getTemplateById,
    createTemplate,
    toggleCreateTemplateSucceed
} from "../../slices/templateSlice"
import {
    requestGetTemplate,
    requestGetTemplateById,
    requestGetTemplateForUser,
    requestPostTemplate,
} from "../requests/template"
import { message } from "antd"

function* handleGetTemplateList(action) {
    try {
        const response = yield call(requestGetTemplate)
        const { data } = response
        yield put(setTemplates(data.result))
    } catch (error) {
        console.log(error)
    }
}

function* onHandleGetTemplateList() {
    yield takeLatest(getTemplates.type, handleGetTemplateList)
}

function* handleGetTemplateListForUser(action) {
    try {
        const response = yield call(requestGetTemplateForUser)
        const { data } = response
        yield put(setTemplatesForUser(data.result))
    } catch (error) {
        console.log(error)
    }
}
function* onHandleGetTemplateListForUser() {
    yield takeLatest(getTemplatesForUser.type, handleGetTemplateListForUser)
}

function* handleGetTemplateById(action) {
    try {
        const response = yield call(requestGetTemplateById, action.payload)
        const { data } = response
        yield put(setTemplateById(...data.data))
    } catch (error) {
        console.log(error)
    }
}

function* onHandleGetTemplateById() {
    yield takeLatest(getTemplateById.type, handleGetTemplateById)
}

function* handleCreateTemplate(action) {
    try {
        const response = yield call(requestPostTemplate, action.payload)
        if (response.status === 200) {
            yield put(toggleCreateTemplateSucceed())
            message.success("Tao thanh cong")
        }
    } catch (error) {
        console.log(error)
    }
}

function* onHandleCreateTemplate() {
    yield takeLatest(createTemplate.type, handleCreateTemplate)
}

export const templateSaga = [
    fork(onHandleGetTemplateList),
    fork(onHandleGetTemplateById),
    fork(onHandleCreateTemplate),
    fork(onHandleGetTemplateListForUser)
]
