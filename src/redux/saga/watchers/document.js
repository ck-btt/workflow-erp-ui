import { message } from "antd"
import { call, put, takeLatest, fork } from "redux-saga/effects"
import {
    getDocsSentByMe,
    getDocsSentToMe,
    setDocsSentByMe,
    setDocsSentToMe,
    submitTemplate,
} from "../../slices/documentSlice"
import {
    requestGetDocsSentByme,
    requestGetDocsSentTome,
    requestPostSubmitDocument,
} from "../requests/document"

function* handleGetDocsSentByme(action) {
    try {
        const response = yield call(requestGetDocsSentByme)
        yield put(setDocsSentByMe(response.data.list_doc))
    } catch (error) {
        // console.log(error)
    }
}

function* handleGetDocsSentTome(action) {
    try {
        const response = yield call(requestGetDocsSentTome)
        yield put(setDocsSentToMe(response.data.list_doc))
    } catch (error) {
        // console.log(error)
    }
}

function* handlePostSubmitDocument(action) {
    try {
        console.log(action)
        const response = yield call(requestPostSubmitDocument, action.payload)
        if (response.status === 200) {
            message.success("Gui thanh cong")
            // const response = yield call(requestGetFolder)
            // const { data } = response
            // yield put(setFolder(data.message))
        }
    } catch (error) {
        message.error("Gui that bai")
        console.log(error)
    }
}

function* documentSagaWatcher() {
    yield takeLatest(getDocsSentByMe.type, handleGetDocsSentByme)
    yield takeLatest(getDocsSentToMe.type, handleGetDocsSentTome)
    yield takeLatest(submitTemplate.type, handlePostSubmitDocument)
}

export const documentSaga = [fork(documentSagaWatcher)]
