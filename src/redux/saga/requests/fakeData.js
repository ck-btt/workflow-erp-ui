import axios from "axios"

export function requestGetParts(search) {
    return axios.request({
        method: "get",
        url:
            process.env.HD_EXP_DOMAIN +
            "/sampledata/part/list" +
            (search ? `?kw=${search}` : ""),
    })
}

export function requestGetPositions() {
    return axios.request({
        method: "get",
        url: process.env.HD_EXP_DOMAIN + "/sampledata/position/list",
    })
}

export function requestGetUsers() {
    return axios.request({
        method: "get",
        url: process.env.HD_EXP_DOMAIN + "/sampledata/user/list",
    })
}
