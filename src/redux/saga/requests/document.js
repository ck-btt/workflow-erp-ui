import axios from "axios"

export function requestGetDocsSentByme() {
    // get cookie
    let decodedCookies = decodeURIComponent(document.cookie)
    let cookies = decodedCookies.split("; ")
    let token
    for (const i of cookies) {
        if (i.startsWith("token")) {
            token = i.split("=")[1]
        }
    }
    return axios.request({
        method: "get",
        headers: {
            Authorization: `Bearer ${token}`,
        },
        url: process.env.HD_EXP_DOMAIN + "/me/sentby",
    })
}

export function requestGetDocsSentTome() {
    // get cookie
    let decodedCookies = decodeURIComponent(document.cookie)
    let cookies = decodedCookies.split("; ")
    let token
    for (const i of cookies) {
        if (i.startsWith("token")) {
            token = i.split("=")[1]
        }
    }
    return axios.request({
        method: "get",
        headers: {
            Authorization: `Bearer ${token}`,
        },
        url: process.env.HD_EXP_DOMAIN + "/me/sendto",
    })
}

export function requestPostSubmitDocument(data) {
    let decodedCookies = decodeURIComponent(document.cookie)
    let cookies = decodedCookies.split("; ")
    let token
    for (const i of cookies) {
        if (i.startsWith("token")) {
            token = i.split("=")[1]
        }
    }
    return axios.request({
        method: "post",
        headers: {
            Authorization: `Bearer ${token}`,
        },
        data: data,
        url: process.env.HD_EXP_DOMAIN + "/me/doc/submit",
    })
}
