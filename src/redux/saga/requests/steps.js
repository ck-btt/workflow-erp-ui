import axios from "axios"

export function requestGetStepsById(id) {
    return axios.request({
        method: "get",
        url: process.env.HD_EXP_DOMAIN + "/step/workflow/?template_id=" + id,
    })
}
