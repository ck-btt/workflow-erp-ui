import axios from "axios"

export function requestGetFolder() {
    return axios.request({
        method: "get",
        url: process.env.HD_EXP_DOMAIN + "/wm/folder/list",
    })
}

export function requestPostFolder(data) {
    return axios.request({
        method: "post",
        url: process.env.HD_EXP_DOMAIN + "/wm/folder/create",
        data: data,
    })
}
