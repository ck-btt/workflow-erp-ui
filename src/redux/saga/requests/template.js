import axios from "axios"




export function requestGetTemplate() {
    return axios.request({
        method: "get",
        url: process.env.HD_EXP_DOMAIN + "/template/list",
    })


    // return axios.request({
    //     method: "get",
    //     headers: {
    //         Authorization: `Bearer ${token}`
    //     },
    //     url: process.env.HD_EXP_DOMAIN + "/me/templates",
    // })
}

export function requestGetTemplateForUser() {
    // get cookie
    let decodedCookies = decodeURIComponent(document.cookie)
    let cookies = decodedCookies.split('; ');
    let token
    for (const i of cookies) {
        if (i.startsWith('token')) {
            token = i.split('=')[1]
        }
    }
    return axios.request({
        method: "get",
        headers: {
            Authorization: `Bearer ${token}`
        },
        url: process.env.HD_EXP_DOMAIN + "/me/templates",
    })
}

export function requestGetTemplateById(id) {
    return axios.request({
        method: "get",
        url: process.env.HD_EXP_DOMAIN + "/template/list?folder_id=" + id,
    })
}

export function requestPostTemplate(data) {
    return axios.request({
        method: "post",
        url: process.env.HD_EXP_DOMAIN + "/template/create",
        data: data,
    })
}
